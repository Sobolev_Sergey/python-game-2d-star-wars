import pygame
import sys

import pyganim

from game_objects import Player, Background, Plasmoid, Meteorite
from settings import SIZE, BLUE

pygame.init()
pygame.display.set_caption('Game 2D Star Wars')

screen = pygame.display.set_mode(SIZE)                      # высота и ширина экрана
clock = pygame.time.Clock()                                 # для регулировки частоты кадров

explostion_animation = pyganim.PygAnimation([               # анимация взрыва
    ('assets/blue_explosion/1_{}.png'.format(i), 50) for i in range(17)
], loop=False)                                              # отключаемповторение

music = pygame.mixer.Sound('assets/music/game.wav')         #добавляем музыку - фон
music.play(-1)

# Groups
all_objects = pygame.sprite.Group()                         # группа объектов: фон, игрок...
plasmoids = pygame.sprite.Group()                           # группа объектов выстрелы
meteors = pygame.sprite.Group()                             # группа объектов метеоры

explosions = []                                             # массив взрывов

#Game_objects
player = Player(clock, plasmoids)                           # загружаем игрока и выстрелы
background = Background()                                   # загружаем фон

# add objects
all_objects.add(background)                                 # доюавим в группу фон
all_objects.add(player)                                     # добавим в группу игрока
#all_objects.add(Plasmoid(player.rect.midtop))               # добавим появление выстрела там где сейчас игрок
#all_objects.add(Meteorite)                                  # добавим в группу метеориты


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)


    screen.fill(BLUE)                                       # заполним окном цветом

    Meteorite.process_meteors(clock, meteors)

    #player.update()                                        # обновление движение игрока
    #background.update()                                    # обновление движение фона
    all_objects.update()                                    # обновляем группу
    plasmoids.update()
    meteors.update()

    # уничтожает пересекающиеся элементы с анимацией
    meteors_and_plasmoids_collided = pygame.sprite.groupcollide(meteors, plasmoids, True, True)
    for collided in meteors_and_plasmoids_collided:
        explosion = explostion_animation.getCopy()         # создаем копию
        explosion.play()                                   # запускаем анимацию
        explosions.append((explosion, (collided.rect.center)))

    player_and_meteors_collided = pygame.sprite.spritecollide(player, meteors, False)
    if player_and_meteors_collided:
        all_objects.remove(player)

    #screen.blit(background.image, background.rect)          # выводим фон на экран
    #screen.blit(player.image, player.rect)                  # выводим игрока на экран
    all_objects.draw(screen)
    plasmoids.draw(screen)
    meteors.draw(screen)

    for explosion, position in explosions.copy():           # щтрисовка анимации взрыва
        if explosion.isFinished():                          # если она закончилась
            explosions.remove((explosion, position))                   # то удадяем ее из памяти
        else:
            x, y = position                                 # координаты взрыва
            explosion.blit(screen, (x - 128, y - 128))

    pygame.display.flip()                                   # вывести изменения на экран
    clock.tick(30)                                          # ограничение в количестве кадров в секунду
