# все игровые объекты
import random

import pygame

from settings import WIDTH, HEIGHT


class Player(pygame.sprite.Sprite):
    max_speed = 10                                                  # скорость игрока
    shooting_cooldown = 200                                         # паузы между выстрелами

    def __init__(self, clock, plasmoids):
        super(Player, self).__init__()

        self.clock = clock                                          # таймауд задержка выстрелов
        self.plasmoids = plasmoids                                  # выстрелы

        self.image = pygame.image.load('assets/player.png')         # изображение игрока
        self.rect = self.image.get_rect()                           # размеры игрока

        self.rect.centerx = WIDTH / 2                               # показываем по горизонтали по центру
        self.rect.bottom = HEIGHT - 10                              # по вертикали внизу

        self.current_speed = 0                                      # изменение скорости
        self.current_shooting_cooldown = 0                          # текущее состояние стрельбы

        self.plasmoid_sound = pygame.mixer.Sound('assets/sounds/plasma_bolt.wav')
        self.player_sound = pygame.mixer.Sound('assets/sounds/player_explosion.wav')

    def update(self):                                               # следим за игроком
        keys = pygame.key.get_pressed()                             # отслеживаем нажатия клавиш

        if keys[pygame.K_LEFT]:                                     # нажал лево # print('Left')
            self.player_sound.play()
            self.current_speed = - self.max_speed
        elif keys[pygame.K_RIGHT]:                                  # нажал право # print('Right')
            self.player_sound.play()
            self.current_speed = self.max_speed
        else:
            self.current_speed = 0

        self.rect.move_ip((self.current_speed, 0))                  # обновляем изображение, смещение
        self.process_shooting()                                     # стреляем

    def process_shooting(self):                                     # выстрелы
        keys = pygame.key.get_pressed()                             # отслеживаем нажатия клавиш

        if keys[pygame.K_SPACE] and self.current_shooting_cooldown <= 0:       # стреляем пробелом
            self.plasmoid_sound.play()
            self.plasmoids.add(Plasmoid(self.rect.midtop))          # стреляем, передаем координаты игрока
            self.current_shooting_cooldown = self.shooting_cooldown
        else:
            self.current_shooting_cooldown -= self.clock.get_time()

        for plasmoid in list(self.plasmoids):                       # удаляем плазму когда выходит за границы экрана
            if plasmoid.rect.bottom < 0:
                self.plasmoids.remove(plasmoid)


class Background(pygame.sprite.Sprite):                             # фон
    def __init__(self):
        super(Background, self).__init__()

        self.image = pygame.image.load('assets/background.png')     # изображение фона
        self.rect = self.image.get_rect()                           # размеры фона

        self.rect.bottom = HEIGHT                                   # фон по высоте экрана

    def update(self):                                               # движение фона
        self.rect.bottom += 5                                       # скорость движения экрана

        if self.rect.bottom >= self.rect.height:                    # бесконечный фон
            self.rect.bottom = HEIGHT


class Plasmoid(pygame.sprite.Sprite):                               # выстрелы плазмы
    speed = -30                                                     # скорость выстрела

    def __init__(self, position):
        super(Plasmoid, self).__init__()

        self.image = pygame.image.load('assets/plasmoid.png')       # изображение снаряда-выстрела
        self.rect = self.image.get_rect()                           # размеры снаряда-выстрела

        self.rect.midbottom = position                              # появдяется там где игрок

    def update(self):
        self.rect.move_ip((0, self.speed))

class Meteorite(pygame.sprite.Sprite):                              # враги метеориты
    cooldown = 250
    current_cooldown = 0
    speed = 10

    def __init__(self):
        super(Meteorite, self).__init__()

        image_name = 'assets/meteor{}.png'.format(random.randint(1, 8))

        self.image = pygame.image.load(image_name)
        self.rect = self.image.get_rect()

        self.rect.midbottom = (random.randint(0, WIDTH), 0)
        self.asteroid_sound = pygame.mixer.Sound('assets/sounds/asteroid_explosion.wav')

    def update(self):
        #self.asteroid_sound.play()
        self.rect.move_ip((0, self.speed))

    @staticmethod
    def process_meteors(clock, meteorites):                         # постоянные метеориты
        if Meteorite.current_cooldown <= 0:
            meteorites.add(Meteorite())                             # то создаем новый метеорит
            Meteorite.current_cooldown = Meteorite.cooldown
        else:
            Meteorite.current_cooldown -= clock.get_time()

        for m in list(meteorites):
            if (m.rect.right < 0 or m.rect.left > WIDTH or m.rect.top > HEIGHT):
                meteorites.remove(m)
